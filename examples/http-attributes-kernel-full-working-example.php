<?php

// use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
// use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollectionBuilder;

// use SymfonyPlayground\Component\HttpKernel\HttpKernel;

use function Functional\partial_left;

// use function SymfonyUtil\Component\HttpFoundation\send;

// replace with file to your own project bootstrap
if (file_exists(__DIR__ . '/../vendor/autoload.php')):
    include __DIR__ . '/../vendor/autoload.php';
else:
    require '/usr/share/php/Symfony/Component/EventDispatcher/autoload.php';
    require '/usr/share/php/Symfony/Component/Routing/autoload.php';
endif;

$request = Request::createFromGlobals();

// $dispatcher = new EventDispatcher();
// Why not fluid?
// $dispatcher->addSubscriber(new RouterListener(
    // new UrlMatcher(
        // (new RouteCollectionBuilder())
            // ->addRoute(new Route('/hello/{name}', [
                // '_controller' => function (Request $request) {
                    // return new Response(
                        // sprintf("Hello %s", $request->get('name'))
                    // );
                // }
            // ]))
            // ->build()
        // ,
        // new RequestContext()
    // )
    // ,
    // new RequestStack()
// ));

$route_collection = (new RouteCollectionBuilder())
    ->addRoute(new Route('/hello/{name}', [
        '_controller' => function (Request $request) {
            return new Response(
                sprintf("Hello %s", $request->get('name'))
            );
        }
    ]))
    ->build()
;

$url_matcher = (new UrlMatcher(
    $route_collection,
    new RequestContext()
));

// https://github.com/symfony/http-kernel/blob/master/EventListener/RouterListener.php
$attributes = function (
    RequestMatcherInterface $url_matcher,
    Request $request
):Request {
    $request = clone $request;
    $parameters = $url_matcher->matchRequest($request);
    $request->attributes->add($parameters);
    unset($parameters['_route'], $parameters['_controller']);
    $request->attributes->set('_route_params', $parameters);
    return $request;
};

// $kernel = new HttpKernel(
    // $dispatcher,
    // new ControllerResolver(),
    // new RequestStack(),
    // new ArgumentResolver()
// );

function http_kernel(
    callable $controller_from_request,
    callable $argument,
    callable $request_event,
    Request $request
): Response {
    $request = $request_event($request);
    $controller = $controller_from_request($request);
    return $controller(...$argument($request, $controller));
}

// $kernel->terminate(
    // $request,
    // send($kernel->handle($request))
    // // ($kernel->handle($request))->send()
// );

http_kernel(
    [(new ControllerResolver()), 'getController'],
    [(new ArgumentResolver()), 'getArguments'],
    partial_left($attributes, $url_matcher),
    $request
)->send();

