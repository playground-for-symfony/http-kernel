# http-kernel

Structured process for converting a Request into a Response

# Basic website features
## Cookies
* [*The HttpFoundation Component*
  ](https://symfony.com/doc/current/components/http_foundation.html)
* [*New in Symfony 3.3: Cookie improvements*
  ](https://symfony.com/blog/new-in-symfony-3-3-cookie-improvements)
  2016-12 Javier Eguiluz

### PHP or Symfony Session
#### PHP
* [*Session Handling*](https://www.php.net/manual/en/book.session.php)
* [*$_SESSION*](https://www.php.net/manual/en/reserved.variables.session.php)
* [*PHP Sessions*
  ](https://www.w3schools.com/php/php_sessions.asp)
  w3schools.com

#### Symfony
* [*The HttpFoundation Component*
  ](https://symfony.com/doc/current/components/http_foundation.html)
* [*Sessions*
  ](https://symfony.com/doc/master/session.html)
* [*Store Sessions in a Database*
  ](https://symfony.com/doc/master/session/database.html)

#### Usage of session
* [*The Form Component*
  ](https://symfony.com/doc/current/components/form.html)